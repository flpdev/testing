#!/bin/bash
cd /home/ferran/Ifac/projects/launcher/out/target/product/msm8953_64
adb reboot bootloader
fastboot flash aboot emmc_appsboot.mbn
fastboot flash boot boot.img
fastboot flash cache cache.img
fastboot flash dtbo dtbo.img
fastboot flash mdtp mdtp.img
fastboot flash persist persist.img
fastboot flash recovery recovery.img
fastboot flash system system.img
fastboot flash userdata userdata.img
fastboot flash vbmeta vbmeta.img
fastboot flash vendor vendor.img
fastboot reboot
